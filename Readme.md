<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
<uses-permission android:name="android.permission.READ_PHONE_STATE"/>

### AccessBlocking ###
	
	//- Use inside Activity
		AccessBlocking ac = AccessBlocking.getInstance(SplashActivity.this, nextIntent, previousIntent);	//-- will nextIntent after checked. , previousIntent not effect.
		ac.checking(getString(R.string.access_blocking_appname), versionName, TrueAppUtility.getIMEI(SplashActivity.this), "android");


	//- Use outside Activity (Service, BroadcastReceiver, Widget)
		
		<activity android:name="com.truelife.mobile.android.access_blocking.activity.DialogActivity" android:launchMode="singleTop" android:theme="@android:style/Theme.Dialog"/>
		<activity android:name="com.truelife.mobile.android.access_blocking.activity.DialogWebSheetActivity" android:launchMode="singleTop" android:theme="@android:style/Theme.Dialog"/>
		 
		 AccessBlockingForService ac = new AccessBlockingForService(context);
		 ac.checking(context.getString(R.string.access_blocking_appname), versionName, TrueAppUtility.getIMEI(context), "android");
	 
	 
	 
### Internal Stat ###  
- Step 1: init stat 

	/*
	 *	Function startStatistic(Activity activity_context,String cachename,String ssoid,String productid,String app_version)
	 *
	 *
	 *		## How to use ##
	 *			: Statistic.startStatistic(HomeActivity.this, getString(R.string.cache_name), login_config.getSSOID(), getString(R.string.app_id), getString(R.string.app_version));
	 */
	
- Step 2: Tracking
	/*
	 *	Function allTracking(String pagename,String referrerurl,String contentid,String sourceofcontent,String titlename)
	 *	
	 *
	 *		## How to use ##
	 *			 : Statistic.allTracking("pagename1","referrerurl","contentid","sourceofcontent","titlename1");
	 */ 
		--> 
		
	/*
	 *	Function tracking(String pagename,String titlename)
	 *	
	 *	
	 *
	 *		## How to use ##   
	 *			: Statistic.tracking("pagename2","titlename2")
	 */ 

	 
	 
	 
	 ///////
	 
	 
	