package com.truelife.mobile.android.access_blocking.activity;

import java.util.HashMap;
import java.util.List;

import com.truelife.mobile.android.access_blocking.util.LOG;
import com.truelife.mobile.android.util.file.SerializerObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;

public class DialogWebSheetActivity extends Activity {

	protected String TAG = "Blocking.DialogActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		Bundle b = getIntent().getExtras();
		
		if(b != null){
			
			String url =  b.getString("url");
			
			Display display = getWindowManager().getDefaultDisplay(); 
		    int width = display.getWidth();
		    int height = display.getHeight();
		        
			LinearLayout content = new LinearLayout(getApplicationContext());
	        content.setOrientation(LinearLayout.VERTICAL);
	        //content.setBackgroundColor(Color.WHITE);
	        content.setLayoutParams(new LayoutParams(width, height));
	        
	        content.setGravity(Gravity.CENTER);
	        
	        WebView body = new WebView(getApplicationContext());

	        
	        body.setLayoutParams(new LayoutParams(width, height));
	        //body.setPadding(10, 10, 10, 10);
	        body.setInitialScale(100);
	        
	        //body.setBackgroundColor(0);
	        body.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
	        
	        body.getSettings().setJavaScriptEnabled(true);
	        body.setWebViewClient(new WebViewClient(){
	        	//@Override
	        	public boolean shouldOverrideUrlLoading(WebView view, String url) {
	        		
	        		LOG.e("url", url);
	        		
	        		if(url.equals("http://app/?close")){
	                	finish();
	        	   	}else if(url.contains("http://app/?terminate")){
	        	   		System.exit(0);
	        	   	}else if(url.contains("http://app/?browser")){
	        	   		String urls = url.replace("http://app/?browser&url=", "");
	        	   		
	        	   		if( !urls.startsWith("market://" ) &&  !urls.startsWith("http")  ){
							urls = "http://"+urls;
						}

	            		LOG.e("urls", urls);
	        	   		
	        	   		Intent intent = new Intent(Intent.ACTION_VIEW);
	        	   		intent.setData(Uri.parse(urls));
	        	   		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    				startActivity(intent);
	    				System.exit(0);
	        	    }else{
	        	    	view.loadUrl(url);
	        	    }
	        		return true;
	        	}
	        });
	        
	        LOG.e("url", url);
	        
	        body.loadUrl(url);
	        
	        content.addView(body);
			
			setContentView(content);
		}else{
			finish();
		}
		
		
	}
	
}
