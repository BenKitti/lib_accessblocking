package com.truelife.mobile.android.access_blocking.activity;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.truelife.mobile.android.access_blocking.R;
import com.truelife.mobile.android.access_blocking.util.LOG;
import com.truelife.mobile.android.util.file.SerializerObject;

public class DialogWebview extends Activity{

	private WebView webviewContent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setTheme(android.R.style.Theme_Dialog);
		setContentView(R.layout.webview_dialog);
		
		webviewContent = (WebView)findViewById(R.id.webviewContent);
		webviewSetting();
		
		findViewById(R.id.btnClose).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				finish();
			}
		});
		
		
		Bundle b = getIntent().getExtras();
		
		if(b != null){
			
			String url =  b.getString("url");
			
			webviewContent.loadUrl(url);
			
		}
		
		 
		
	}
	
	private void webviewSetting() {
		
		
//		webviewContent.clearCache(true);
		webviewContent.setInitialScale(100);
		webviewContent.getSettings().setJavaScriptEnabled(true);
		webviewContent.getSettings().setPluginState(PluginState.ON);
		webviewContent.getSettings().setGeolocationEnabled(true);
		webviewContent.getSettings().setDomStorageEnabled(true);
		webviewContent.getSettings().setDatabaseEnabled(true);
		webviewContent.getSettings().setAllowFileAccess(true);
		webviewContent.getSettings().setDefaultTextEncodingName("UTF-8");
		webviewContent.getSettings().setDomStorageEnabled(true);
		webviewContent.getSettings().setLoadWithOverviewMode(true);
		webviewContent.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
		webviewContent.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		
		webviewContent.setVerticalScrollBarEnabled(true);
		webviewContent.setVerticalScrollbarOverlay(true);
		webviewContent.setScrollContainer(true);
		webviewContent.setBackgroundColor(0x00000000);
		
		webviewContent.setWebViewClient(new WebViewClient() {
			
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				
				if(url.equals("http://app/?close")){
                	finish();
        	   	}else if(url.contains("http://app/?terminate")){
        	   		System.exit(0);
        	   	}else if(url.contains("http://app/?browser")){
        	   		String urls = url.replace("http://app/?browser&url=", "");
        	   		
        	   		if( !urls.startsWith("market://" ) &&  !urls.startsWith("http")  ){
						urls = "http://"+urls;
					}

            		LOG.e("urls", urls);
        	   		
        	   		Intent intent = new Intent(Intent.ACTION_VIEW);
        	   		intent.setData(Uri.parse(urls));
        	   		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    				startActivity(intent);
    				finish();
        	    }else{
        	    	view.loadUrl(url);
        	    }
				
				
				return true;
			}
			
			
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				if(errorCode != 200) {
					finish();
				}
			}
			
		});
	}
}
