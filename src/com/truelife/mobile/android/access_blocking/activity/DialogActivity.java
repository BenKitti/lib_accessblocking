package com.truelife.mobile.android.access_blocking.activity;

import java.util.HashMap;
import java.util.List;

import com.truelife.mobile.android.util.file.SerializerObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;

public class DialogActivity extends Activity {

	protected String TAG = "Blocking.DialogActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		Bundle b = getIntent().getExtras();
		
		if(b != null){
			
			HashMap<String, Object> control_data =  (HashMap<String, Object>)SerializerObject.deserializeObject((byte []) b.get("control_data"));
			
			LinearLayout content = new LinearLayout(getBaseContext());
			content.setOrientation(LinearLayout.VERTICAL);
			content.setBackgroundColor(Color.WHITE);
			content.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
			
			TextView title = new TextView(getBaseContext());
			title.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
			title.setTextColor(Color.parseColor("#FFFFFF"));
			title.setBackgroundColor(Color.parseColor("#000000"));
			title.setPadding(10, 10, 10, 10);
			
			title.setText(String.valueOf(control_data.get("title")));
			
			content.addView(title);
			
			TextView body = new TextView(getBaseContext());
			body.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
			body.setTextColor(Color.parseColor("#000000"));
			body.setBackgroundColor(Color.parseColor("#FFFFFF"));
			body.setPadding(10, 10, 10, 10);
			
			body.setText(String.valueOf(control_data.get("url")));
			
			content.addView(body);
			
			LinearLayout control_content = new LinearLayout(getBaseContext());
			control_content.setOrientation(LinearLayout.HORIZONTAL);
			control_content.setGravity(Gravity.CENTER);
			control_content.setBackgroundColor(Color.WHITE);
			control_content.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
			
			content.addView(control_content);
			
			try {
				List<HashMap<String, Object>> button_data = (List<HashMap<String, Object>>) control_data.get("button");
				
				if(button_data!=null){
					for(final HashMap<String, Object> button:button_data){
						Button button_click = new Button(getBaseContext());
						
						button_click.setText(String.valueOf(button.get("caption")));
						
						button_click.setOnClickListener(new OnClickListener() {
							
							public void onClick(View v) {
								if(String.valueOf(button.get("type")).equals("terminate")){
									System.exit(0);
								}else if(String.valueOf(button.get("type")).equals("browser")){
									Intent intent = new Intent(Intent.ACTION_VIEW);
									String urls = String.valueOf(button.get("param")).replace("url=", "");
									
									if( !urls.startsWith("market://" ) &&  !urls.startsWith("http")  ){
										urls = "http://"+urls;
									}
									
									intent.setData(Uri.parse(urls));
									intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(intent);
									
								}else if(String.valueOf(button.get("type")).equals("websheet")){
//								dialog.dismiss();
//									PopUpWebSheet(String.valueOf(button.get("param")).replace("url=", ""));
									Log.d(TAG , "Type = websheet");
								} 
							}
						});
						
						control_content.addView(button_click);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			setContentView(content);
		}else{
			finish();
		}
		
		
	}
	
}
