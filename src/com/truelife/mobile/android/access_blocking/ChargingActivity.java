package com.truelife.mobile.android.access_blocking;

import com.truelife.mobile.android.access_blocking.util.AccessBlocking;
import com.truelife.mobile.android.access_blocking.util.LOG;


import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.Window;

public class ChargingActivity extends Activity {

	Bundle bundle;
		
	
	public Activity previousActivity;
	
	public static ChargingActivity ca;
	
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        bundle = this.getIntent().getExtras();
        String url = bundle.getString("url");
        
        ChargingPageDialog fbDialog = new ChargingPageDialog(this,url);
        setContentView(fbDialog);
                
        fbDialog.show();
        
        ca = this;
    }
    
    public void setPrevious(Activity context){
    	previousActivity = context;
    }
    
    public boolean onKeyDown(int keyCode, KeyEvent event) {
		boolean r = false;
		LOG.v("key", "" + 555);
		if (keyCode == 4) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			ChargingActivity.this.finish();
			AccessBlocking.charging.context.finish();
			System.exit(0);
			r = true;
		}
		return r;
	}
}