/*
 * Copyright 2009 Codecarpet
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.truelife.mobile.android.access_blocking;

import java.net.MalformedURLException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.truelife.mobile.android.access_blocking.util.AccessBlocking;
import com.truelife.mobile.android.access_blocking.util.LOG;

public class ChargingDialog extends FrameLayout {


    // /////////////////////////////////////////////////////////////////////////////////////////////////


    protected WebView _webView;

    private LinearLayout content;
    protected Activity mContext;


    private void postDismissCleanup() {
        mContext.finish();
    }

    private void dismiss() {
    	postDismissCleanup();
    }

    public ChargingDialog(Activity context) {
        super(context);

        mContext = context;
        // main content of popup window
        content = new LinearLayout(context);
        content.setOrientation(LinearLayout.VERTICAL);
        content.setBackgroundColor(Color.WHITE);
        content.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        
        _webView = new WebView(context);_webView.setLayoutParams(new LayoutParams(280, 400));
        _webView.setWebViewClient(new WebViewClientImpl());

        
        _webView.setVerticalScrollBarEnabled(false);
        WebSettings webSettings = _webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDefaultTextEncodingName("UTF-8");

        _webView.requestFocus();
        
        content.addView(_webView);

        addView(content);
        _webView.setVisibility(View.INVISIBLE);
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////////
    // WebViewClient

    private final class WebViewClientImpl extends WebViewClient {
    	
    	private Dialog dialog = null;
    	
        //@Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            LOG.v("FBDialog shouldOverrideUrlLoading", "Web view URL = " + url);
            if(url.equals("http://app/?cancel")){
            	AccessBlocking.charging.context.finish();
            	mContext.finish();
    	   		System.exit(0);
    	   	}else if(url.contains("http://app/?success")){
    	   		mContext.startActivity(AccessBlocking.charging.nextIntent);
    	   		mContext.finish();
    	   		AccessBlocking.charging.context.finish();
    	   	}else if(url.contains("tel://")){
    	   		_webView.setEnabled(false);
    	   		final String tel = url.substring(6,url.length());
    	   		LOG.v("shouldOverrideUrlLoading on truehit:",tel);
    			Intent i = new Intent(Intent.ACTION_CALL);
    			String phoneId = tel;
    			i.setData(Uri.parse("tel:" + phoneId));
    			mContext.startActivity(i);
    			AccessBlocking.charging.state = "tel";	
    	    }else{
    	    	_webView.loadUrl(url);
    	    }
            return true;
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
        	super.onPageStarted(view, url, favicon);
        		LOG.v("FBDialog onPageStarted", "Web view URL = " + url);
        }
        
        //@Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            LOG.v("FBDialog onPageFinished", "Web view URL = " + url);

            _webView.setVisibility(View.VISIBLE);
        }

    }

    public String getTitle() {
        return null;//_titleLabel.getText().toString();
    }

    public void setTitle(String title) {
       // _titleLabel.setText(title);
    }

    public void show() {
        load();
    }

    protected void load() {
        // Intended for subclasses to override
    }

    protected void loadURL(String url) throws MalformedURLException {
    	_webView.loadUrl(url);
    }
}
