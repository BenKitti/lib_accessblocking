package com.truelife.mobile.android.access_blocking.util;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.http.HttpEntity; 
import org.apache.http.HttpResponse; 
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion; 
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient; 
import org.apache.http.client.methods.HttpPost; 
import org.apache.http.entity.mime.MultipartEntity; 
import org.apache.http.entity.mime.content.ContentBody; 
import org.apache.http.entity.mime.content.FileBody; 
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient; 
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.kxml2.io.KXmlParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.annotation.TargetApi;
import android.os.AsyncTask;

@TargetApi(3)
public class Uploadfile extends AsyncTask<String, Void, Void>{
	
	private String TAG = "InternalState";

	@Override
	protected Void doInBackground(String... params1) {
		// TODO Auto-generated method stub
		HttpClient httpclient = new DefaultHttpClient(); 
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		
	    HttpPost httppost = new HttpPost("http://stat.adapter.truelife.com/rest/?method=uploadStat"); 
	    httppost.setParams(params);
	    File folder = new File(params1[0]); 
	    File filename[] = folder.listFiles();
	    if(filename.length!=0){
			for(int i=0;i<filename.length;i++){
			    try 
				{
			    	MultipartEntity mpEntity = new MultipartEntity(); 
				    ContentBody filebody = new FileBody(filename[i],"text/plain" ); 
				    
				    String udid = params1[1] == null ? "0" : params1[1];
				    String sso_id = params1[2] == null ? "n/a" : params1[2];
				    
				    LOG.d(TAG, "sso_id : " + sso_id);
				    LOG.d(TAG, "udid : " + udid);
				    
				    mpEntity.addPart("stat_file", filebody);
				    mpEntity.addPart("udid", new StringBody(udid));
				    mpEntity.addPart("sso_id", new StringBody(sso_id));
				    mpEntity.addPart("device", new StringBody("android"));
				    
				    httppost.setEntity(mpEntity); 
				    System.out.println("executing request " + httppost.getRequestLine()); 
				    HttpResponse response = httpclient.execute(httppost); 
				    if( response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
					{
				    	HttpEntity resEntity = response.getEntity(); 
		 
				    	if (resEntity != null) { 
				   	      	String str = EntityUtils.toString(resEntity);
				   	      
				   	      	if(str.charAt(0)!='<'){
				   	      		str = str.substring(1,str.length());
				   	      	}
				   	      	
				   	      	KXmlParser parserXML = new KXmlParser();
				   	      	parserXML.setInput(new ByteArrayInputStream(str.getBytes("UTF-8")),"UTF-8"); 
				   	      	HashMap<String, String> parser_data = new HashMap<String, String>();
				   	      
				   	      	int intEventParse = 0;
					   	    while ((intEventParse = parserXML.next()) != XmlPullParser.END_DOCUMENT) {
					   	    	if (intEventParse == XmlPullParser.START_TAG) {
					   	    		if(parserXML.getName().equals("response")){
					   	    			parserXML.require(XmlPullParser.START_TAG,null, "response");
					   	    		}
					   	    		else if (parserXML.getName().equals("code")) {
					   	    			parserXML.require(XmlPullParser.START_TAG,null, "code");
					   	    			parser_data.put("code",parserXML.nextText());
					   	    			parserXML.require(XmlPullParser.END_TAG,null, "code");
					   	    		} else if (parserXML.getName().equals("description")) {
					   	    			parserXML.require(XmlPullParser.START_TAG,null, "description");
					   	    			parser_data.put("description",parserXML.nextText());
					   	    			parserXML.require(XmlPullParser.END_TAG,null, "description");
					   	    		} else if (parserXML.getName().equals("method")) {
					   	    			parserXML.require(XmlPullParser.START_TAG,null, "method");
					   	    			parser_data.put("update",parserXML.nextText());
					   	    			parserXML.require(XmlPullParser.END_TAG,null, "method");
					   	    		} else if (parserXML.getName().equals("execute_time")) {
					   	    			parserXML.require(XmlPullParser.START_TAG,null, "execute_time");
					   	    			parser_data.put("allow",parserXML.nextText());
					   	    			parserXML.require(XmlPullParser.END_TAG,null, "execute_time");
					   	    		}
					   	    	}
					   	    }
					   	    if(parser_data.get("code").equals("200")){
					   	    	System.out.println("Code : "+parser_data.get("code")+" Description : "+parser_data.get("description"));
					   	    	filename[i].delete();
					   	    }
					   	    else{
					   	    	System.out.println("Code : "+parser_data.get("code")+" Description : "+parser_data.get("description"));
					   	    }
				   	    }
					}
				}
			    catch (ClientProtocolException e) {
			    	LOG.e(TAG , "Can't upload, ClientProtocolException", e);
				}
				catch (IOException e) {
					LOG.e(TAG , "Can't upload, IOException", e);
				}
			    catch (XmlPullParserException e) {
			    	LOG.e(TAG , "Can't upload, XmlPullParserException", e);
				}catch (Exception e) {
					LOG.e(TAG , "Can't upload, Unknown error", e);
				}
			}
	    }
	    httpclient.getConnectionManager().shutdown();
		return null;
	}
	
}
