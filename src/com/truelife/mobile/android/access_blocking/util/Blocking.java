package com.truelife.mobile.android.access_blocking.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.kxml2.io.KXmlParser;
import org.xmlpull.v1.XmlPullParser;

import android.os.Build;
import android.os.Handler;
import android.os.Message;

public class Blocking {
	
	public static void checkAccessBlocking(final Handler threadHandler,final String appname,final String appversion,final String country,final String carier,final String device,final String uid,final String os){
		new Thread() {
			@Override
			public void run() {
				InputStreamReader isrXML = null;
				Message message = new Message();
				try {
					URLConnection cn = new URL(Param.access_url+"?method=validate&appname="+appname+"&ver="+appversion+"&cc="+country+"&cn="+carier+"&device="+device+"&udid="+uid+"&os="+URLEncoder.encode(Build.VERSION.RELEASE)+"&model="+URLEncoder.encode(Build.MODEL)).openConnection();
					

					LOG.e("url", Param.access_url+"?method=validate&appname="+appname+"&ver="+appversion+"&cc="+country+"&cn="+carier+"&device="+device+"&udid="+uid+"&os="+URLEncoder.encode(Build.VERSION.RELEASE)+"&model="+URLEncoder.encode(Build.MODEL));

					
					cn.setDoInput(true);
					cn.setDoOutput(true);
					cn.setReadTimeout(20000);
					cn.connect();
					KXmlParser parserXML = new KXmlParser();
					isrXML = new InputStreamReader(cn.getInputStream(),"UTF-8");
					
					String str;
					StringBuffer buff = new StringBuffer();
					BufferedReader in = new BufferedReader(
							new InputStreamReader(cn.getInputStream()));
					while ((str = in.readLine()) != null) {
						buff.append(str);
					}
					str = buff.toString();
					
					if(str.charAt(0)!='<'){
						str = str.substring(1,str.length());
					}
					
					parserXML.setInput(new ByteArrayInputStream(str.getBytes("UTF-8")),"UTF-8");
					
					List<HashMap<String, Object>> list_data = new Vector<HashMap<String,Object>>();
					
					HashMap<String, Object> main_data = new HashMap<String, Object>();
					HashMap<String, Object> control_data = null;
					List<HashMap<String, Object>> button_data = null;
					HashMap<String, Object> item_data = null;
					
					int intEventParse = 0;
					while ((intEventParse = parserXML.next()) != XmlPullParser.END_DOCUMENT) {
						if (intEventParse == XmlPullParser.START_TAG) {
							if (parserXML.getName().equals("code")) {
								parserXML.require(XmlPullParser.START_TAG,null, "code");
								main_data.put("code",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "code");
							} else if (parserXML.getName().equals("description")) {
								parserXML.require(XmlPullParser.START_TAG,null, "description");
								main_data.put("description",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "description");
							} else if (parserXML.getName().equals("update")) {
								parserXML.require(XmlPullParser.START_TAG,null, "update");
								main_data.put("update",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "update");
							} else if (parserXML.getName().equals("allow")) {
								parserXML.require(XmlPullParser.START_TAG,null, "allow");
								main_data.put("allow",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "allow");
							} else if (parserXML.getName().equals("credit")) {
								parserXML.require(XmlPullParser.START_TAG,null, "credit");
								main_data.put("credit",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "credit");
							} else if (parserXML.getName().equals("control")) {
								parserXML.require(XmlPullParser.START_TAG,null, "control");
								control_data = new HashMap<String, Object>();
							} else if (parserXML.getName().equals("action")) {
								parserXML.require(XmlPullParser.START_TAG,null, "action");
								control_data.put("action",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "action");
							} else if (parserXML.getName().equals("title")) {
								parserXML.require(XmlPullParser.START_TAG,null, "title");
								control_data.put("title",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "title");
							} else if (parserXML.getName().equals("url")) {
								parserXML.require(XmlPullParser.START_TAG,null, "url");
								control_data.put("url",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "url");
							} else if (parserXML.getName().equals("button")) {
								parserXML.require(XmlPullParser.START_TAG,null, "button");
								button_data = new Vector<HashMap<String,Object>>();
							}  else if (parserXML.getName().equals("item")) {
								parserXML.require(XmlPullParser.START_TAG,null, "item");
								item_data = new HashMap<String, Object>();
							} else if (parserXML.getName().equals("caption")) {
								parserXML.require(XmlPullParser.START_TAG,null, "caption");
								item_data.put("caption",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "caption");
							} else if (parserXML.getName().equals("type")) {
								parserXML.require(XmlPullParser.START_TAG,null, "type");
								item_data.put("type",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "type");
							} else if (parserXML.getName().equals("param")) {
								parserXML.require(XmlPullParser.START_TAG,null, "param");
								item_data.put("param",parserXML.nextText());
								parserXML.require(XmlPullParser.END_TAG,null, "param");
							}
						} else if (intEventParse == XmlPullParser.END_TAG) {
							if (parserXML.getName().equals("item")) {
								parserXML.require(XmlPullParser.END_TAG,null, "item");
								button_data.add(item_data);
							}else if (parserXML.getName().equals("button")) {
								parserXML.require(XmlPullParser.END_TAG,null, "button");
								control_data.put("button",button_data);
							}
						}
					}
					
					list_data.add(main_data);
					if(control_data!=null){
						list_data.add(control_data);
					}
					
					message.what = 200;
					message.obj = list_data;
					threadHandler.sendMessage(message);
				} catch (Exception e) {
//					e.printStackTrace();
//					LOG.e("", e.getMessage().toString());
					
					message.obj = null;
					message.what = 600;
					threadHandler.sendMessage(message);
				} finally {
					try {
						if (isrXML != null) {
							isrXML.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
						message.obj = null;
						message.what = 600;
						threadHandler.sendMessage(message);
					}
				}
			}
		}.start();
	}
}
