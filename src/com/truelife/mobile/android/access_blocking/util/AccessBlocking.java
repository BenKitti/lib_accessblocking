package com.truelife.mobile.android.access_blocking.util;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ActivityManager.RunningServiceInfo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;

import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;

public class AccessBlocking {

	public Activity context;
	
	
	public Intent nextIntent;
	public Intent previousIntent;
	
	public static AccessBlocking charging;
	public String state = "";
	
	Dialog dialog;
	
	int credit = 0;
	boolean check_credit = false;
	
	static private AccessBlocking _instance;
	static public AccessBlocking getInstance(Activity context,Intent nextIntent,Intent previousIntent) {
		if (_instance == null) {
			_instance = new AccessBlocking(context,nextIntent,previousIntent);
		}else{
			_instance.setContext(context);
		}
		return _instance;
	}
	
	
	private AccessBlocking(Activity contextActivity,Intent nextIntent,Intent previousIntent){
		this.context = contextActivity;
		
		this.nextIntent = nextIntent;
		this.previousIntent = previousIntent;
		charging = this;
	}
	
	

	public void setContext(Activity contextActivity){
		this.context = contextActivity;
	}
	
	public void checking(String appname,String version,String IMEI,String agent) {
		
		/*if(!isMyServiceRunning()){
			final Intent intentDeviceTest = new Intent(Intent.ACTION_MAIN);                
			intentDeviceTest.setComponent(new ComponentName("com.test.testaccess","com.test.testaccess.MyService"));
			contextActivity.startService(intentDeviceTest);
		}else{
			LOG.e("Service", "Service Started");
		}*/
		
		String cn = TrueAppUtility.getSimOperator(this.context);
		
		if(String.valueOf(cn).equals("")||String.valueOf(cn).equalsIgnoreCase("null")){
			cn = "unknow";
		}
		
		if(TrueAppUtility.isNetworkAvailable(this.context)){
			Blocking.checkAccessBlocking(threadHandler, URLEncoder.encode(appname), version , URLEncoder.encode(TrueAppUtility.getNetworkCountryIso(context)), URLEncoder.encode(cn) , agent, IMEI, "");
		}else{
			PopUpNotingConnect();
		}
	}
	
	public Handler threadHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case 200:{
					if(msg.obj!=null){
						LOG.e("AccessBlocking", "Data : " + String.valueOf(msg.obj));
						
						
						
						List<HashMap<String, Object>> list_data = (List<HashMap<String, Object>>) msg.obj;
						
						HashMap<String, Object> h = list_data.get(0);
						
						BlockingPreferences blockingPref = null;
						try {
							blockingPref = new BlockingPreferences(context);
							
							//credit = Integer.parseInt(String.valueOf(h.get("credit")));
						} catch (Exception e) {
							//credit = 0;
						}
						credit = 2;
						if(list_data.size()>1){
							
							try {
								String tmpUpdateValue =String.valueOf( list_data.get(0).get("update") );
								
								LOG.d("AccessBlocking", "Last Update Status : " + blockingPref.getBlockingStatus());
								LOG.d("AccessBlocking", "Server Update Status : " + tmpUpdateValue);
								
								if(blockingPref != null && blockingPref.isShouldBlocking(Integer.parseInt(tmpUpdateValue))){
									
									HashMap<String, Object> control_data = list_data.get(1);
									
									if(String.valueOf(control_data.get("action")).equals("alert")){
										PopUpAlert(control_data);
									}else if(String.valueOf(control_data.get("action")).equals("websheet")){
										PopUpWebSheet(String.valueOf(control_data.get("url")).replace("url=", ""));
										
									}else if(String.valueOf(control_data.get("action")).equals("browser")){
										//PopUpWebSheet(String.valueOf(control_data.get("url")));
										Intent intent = new Intent(Intent.ACTION_VIEW);
										String urls = String.valueOf(control_data.get("url")).replace("url=", "");
										
										
										if( !urls.startsWith("market://" ) &&  !urls.startsWith("http")  ){
											urls = "http://"+urls;
										}
										
										intent.setData(Uri.parse(urls));
										intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										context.startActivity(intent);
										System.exit(0);
									}
								}else{
									nextIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									context.startActivity(nextIntent);
									try {									
										context.finish();
									} catch (Exception e) {
									}
								}
								
							} catch (Exception e) { 
								
							}

						}else{
							
							
							
							HashMap<String, Object> main_data = list_data.get(0);
							String tmpUpdateValue =String.valueOf( main_data.get("update") );
							
							LOG.d("AccessBlocking", "Last Update Status : " + blockingPref.getBlockingStatus());
							LOG.d("AccessBlocking", "Server Update Status : " + tmpUpdateValue);
							
							blockingPref.isShouldBlocking(Integer.parseInt(tmpUpdateValue));
							
							checkCode(main_data);
						}
					}else{
						msg.what = 400;
						threadHandler.sendMessage(msg);
					}
				}break;
				case 400:{
					PopUpNotingConnect();
				}break;
				case 600:{
					PopUpNotingConnect();
				}break;
			}
			super.handleMessage(msg);
		}
	};
	
	public void PopUpAlert(HashMap<String, Object> control_data){
		dialog = new Dialog(context);
		
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		LinearLayout content = new LinearLayout(context);
        content.setOrientation(LinearLayout.VERTICAL);
        content.setBackgroundColor(Color.WHITE);
        content.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        
        TextView title = new TextView(context);
        title.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        title.setTextColor(Color.parseColor("#FFFFFF"));
        title.setBackgroundColor(Color.parseColor("#000000"));
        title.setPadding(10, 10, 10, 10);
        
        title.setText(String.valueOf(control_data.get("title")));
        
        content.addView(title);
        
        TextView body = new TextView(context);
        body.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        body.setTextColor(Color.parseColor("#000000"));
        body.setBackgroundColor(Color.parseColor("#FFFFFF"));
        body.setPadding(10, 10, 10, 10);
        
        body.setText(String.valueOf(control_data.get("url")));
        
        content.addView(body);
        
        LinearLayout control_content = new LinearLayout(context);
        control_content.setOrientation(LinearLayout.HORIZONTAL);
        control_content.setGravity(Gravity.CENTER);
        control_content.setBackgroundColor(Color.WHITE);
        control_content.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        
        content.addView(control_content);
		
        try {
        	List<HashMap<String, Object>> button_data = (List<HashMap<String, Object>>) control_data.get("button");
            
            if(button_data!=null){
            	for(final HashMap<String, Object> button:button_data){
            		Button button_click = new Button(context);
            		
            		button_click.setText(String.valueOf(button.get("caption")));
            		
            		button_click.setOnClickListener(new OnClickListener() {
						
						public void onClick(View v) {
							if(String.valueOf(button.get("type")).equals("terminate")){
								System.exit(0);
							}else if(String.valueOf(button.get("type")).equals("browser")){
								Intent intent = new Intent(Intent.ACTION_VIEW);
								String urls = String.valueOf(button.get("param")).replace("url=", "");
								
								if( !urls.startsWith("market://" ) &&  !urls.startsWith("http")  ){
									urls = "http://"+urls;
								}
								
								intent.setData(Uri.parse(urls));
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		        				context.startActivity(intent);
		        				System.exit(0);
							}else if(String.valueOf(button.get("type")).equals("websheet")){
								dialog.dismiss();
								PopUpWebSheet(String.valueOf(button.get("param")).replace("url=", ""));
							}if(String.valueOf(button.get("type")).equals("close")){
								nextIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								context.startActivity(nextIntent);
								try {									
									context.finish();
								} catch (Exception e) {
								}
							}
						}
					});
            		
            		control_content.addView(button_click);
            	}
            }
		} catch (Exception e) {
			// TODO: handle exception
		}
        
		dialog.setContentView(content);

		dialog.setOnKeyListener(new OnKeyListener() {
			
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if(keyCode==4){
					return true;
				}
				return false;
			}
		});
		
		dialog.show();
	}
	
	public Dialog getDialog() {
		return dialog;
	}
	
	public void PopUpWebSheet(String url){
		dialog = new Dialog(context);
//		dialog.setTheme(android.R.style.Theme_Dialog);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        
        Display display = context.getWindowManager().getDefaultDisplay(); 
        int width = display.getWidth();
        int height = display.getHeight();
        
		LinearLayout content = new LinearLayout(context);
        content.setOrientation(LinearLayout.VERTICAL);
        //content.setBackgroundColor(Color.WHITE);
        content.setLayoutParams(new LayoutParams(width, height));
        
        content.setGravity(Gravity.CENTER);
        
        WebView body = new WebView(context);

        
        body.setLayoutParams(new LayoutParams(width, height));
//        body.setPadding(10, 10, 10, 10);
        body.setInitialScale(100);

        body.getSettings().setLoadWithOverviewMode(true);
//        body.getSettings().setUseWideViewPort(true);
        
        body.setBackgroundColor(Color.BLACK);
        body.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        
        body.getSettings().setJavaScriptEnabled(true);
        body.setWebViewClient(new WebViewClient(){
        	//@Override
        	public boolean shouldOverrideUrlLoading(WebView view, String url) {
        		
        		LOG.e("url", url);
        		
        		if(url.equals("http://app/?close")){
                	dialog.dismiss();
                	
                	nextIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    				context.startActivity(nextIntent);
    				try {									
    					context.finish();
    				} catch (Exception e) {
    				}
					
        	   	}else if(url.contains("http://app/?terminate")){
        	   		System.exit(0);
        	   	}else if(url.contains("http://app/?browser")){
        	   		String urls = url.replace("http://app/?browser&url=", "");
        	   		
        	   		if( !urls.startsWith("market://" ) &&  !urls.startsWith("http")  ){
						urls = "http://"+urls;
					}

            		LOG.e("urls", urls);
        	   		
        	   		Intent intent = new Intent(Intent.ACTION_VIEW);
        	   		intent.setData(Uri.parse(urls));
    				context.startActivity(intent);
    				System.exit(0);
        	    }else{
        	    	view.loadUrl(url);
        	    }
        		return true;
        	}
        });
        
        
        body.loadUrl(url);
        
        content.addView(body);

		dialog.setContentView(content);

		dialog.setOnKeyListener(new OnKeyListener() {
			
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if(keyCode==4){
					context.moveTaskToBack(true);
					context.finish();
					//return true;
				}
				return false;
			}
		});
		
		dialog.show();
	}
	
	public void checkCode(HashMap<String, Object> main_data){
		String code = String.valueOf(main_data.get("code"));
		if(code.equals("200")){
			if(!check_credit){
				nextIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(nextIntent);
				try {									
					context.finish();
				} catch (Exception e) {
				}
			}
		}else if(code.equals("203")){
			if(!check_credit){
				nextIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(nextIntent);
				try {									
					context.finish();
				} catch (Exception e) {
				}
			}
		}
	}
	
	
	public void PopUpNotingConnect(){
		dialog = new Dialog(context);

		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		LinearLayout content = new LinearLayout(context);
        content.setOrientation(LinearLayout.VERTICAL);
        content.setBackgroundColor(Color.WHITE);
        content.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        
        TextView title = new TextView(context);
        title.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        title.setTextColor(Color.parseColor("#FFFFFF"));
        title.setBackgroundColor(Color.parseColor("#000000"));
        title.setPadding(10, 10, 10, 10);
        
        title.setText("System");
        
        content.addView(title);
        
        TextView body = new TextView(context);
        body.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        body.setTextColor(Color.parseColor("#000000"));
        body.setBackgroundColor(Color.parseColor("#FFFFFF"));
        body.setPadding(10, 10, 10, 10);
        
        body.setText(Param.error_1);
        
        content.addView(body);
        
        LinearLayout control_content = new LinearLayout(context);
        control_content.setOrientation(LinearLayout.HORIZONTAL);
        control_content.setGravity(Gravity.CENTER);
        control_content.setBackgroundColor(Color.WHITE);
        control_content.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        
        content.addView(control_content);
		
		Button button_click = new Button(context);
		
		button_click.setText("OK");
		
		button_click.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				System.exit(0);
			}
		});
		
		control_content.addView(button_click);
        
		dialog.setContentView(content);

		dialog.setOnKeyListener(new OnKeyListener() {
			
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if(keyCode==4){
					System.exit(0);
					//return true;
				}
				return false;
			}
		});
		
		dialog.show();
	}
	
	public void checkCredit(String appname,String version,String IMEI,String agent){
		credit = credit-1;
		check_credit = true;
		if(credit<=0){
			
			if(TrueAppUtility.isNetworkAvailable(context)){
				checking(appname, version, IMEI, agent);
			}
		}
	}
	
	private boolean isMyServiceRunning() {
	    ActivityManager manager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if ("com.test.testaccess.MyService".equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
}
