package com.truelife.mobile.android.access_blocking.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.location.Location;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;

public class Statistic {
	
	public static Activity context;
	
	private static String fObject_config_path = "";
	private static String fObject_config_name = "";
	
	private static String sso_id = "";
	private static String product_id = "";
	private static String category_id = "0";
	private static String referrer_url = "";
	private static String content_id = "";
	private static String source_of_content = "";
	private static String res = "";
	private static String physical_id = "";
	private static String msisdn = "";
	private static String os_version = "";
	private static String manufacturer = "";
	private static String application_version = "";
	private static String latitude = "0.000000";
	private static String longitude = "0.000000";
	
	static File file = null;
	static File folder = null;

	private static String TAG = "TrueStat";
	

	@TargetApi(4)
	public static void startStatistic(Activity activity_context,String cachename,String ssoid,String productid,String app_version) {
		
		LOG.i("TrueStat", "Stat startStatistic :: ssoid = " + ssoid + ", productid = " + productid );
		
		context = activity_context;
		
		sso_id = ssoid;
		product_id = productid;
		DisplayMetrics dm = new DisplayMetrics();
		context.getWindowManager().getDefaultDisplay().getMetrics(dm);
		res = dm.widthPixels + "x" + dm.heightPixels;
		physical_id = TrueAppUtility.getIMEI(context.getApplicationContext());
		msisdn = TrueAppUtility.getMSISDN(context);
		os_version = android.os.Build.VERSION.RELEASE;
		manufacturer = android.os.Build.MANUFACTURER;
		application_version = app_version;
		
		fObject_config_path = Environment.getExternalStorageDirectory() + "/" + cachename + "/cache/stat";
		fObject_config_name = UtilGenerator.genTransactionId()+".txt";
		
		folder = new File(fObject_config_path);
		if(!folder.exists()){
			folder.mkdirs();
		}
		else{
			//upload file
			Uploadfile up = new Uploadfile();
			try {
				up.execute(new String[] {fObject_config_path,physical_id,sso_id});
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("error", "error upload file ");
			}
		}
		
		file = new File(fObject_config_path,fObject_config_name);
		
		
		
	}

	public static void statWithLocation(Location location) {
		
		String tmpLat = Statistic.latitude;
		String tmpLon = Statistic.longitude;
		
		try {
			Statistic.latitude = String.valueOf(location.getLatitude());
			Statistic.longitude = String.valueOf(location.getLongitude());
		} catch (Exception e) {
			Statistic.latitude = tmpLat;
			Statistic.longitude = tmpLon;
		}
	}
	
	public static void allTracking(String pagename,String referrerurl,String contentid,String sourceofcontent,String titlename){
		
		LOG.i("TrueStat", "Stat allTracking :: pagename = " + pagename + ", Title = " + titlename);
		
		String text = Statistic.sso_id +"|"+ Statistic.product_id +"|"+ Statistic.category_id +"|"+ pagename +"|"+ referrerurl 
				+"|"+ gettime() +"|"+ contentid +"|"+ sourceofcontent +"|"+ Statistic.res +"|"+ Statistic.physical_id +"|"+ 
				titlename +"|"+ Statistic.msisdn +"|"+ Statistic.os_version +"|"+ Statistic.manufacturer +"|"+ 
				Statistic.application_version +"|"+ Statistic.latitude +"|"+ Statistic.longitude +"\n";
		writeFile(text);
		
	}
	
	public static void tracking(String pagename,String titlename){
		
		LOG.i(TAG , "Stat tracking :: pagename = " + pagename + ", Title = " + titlename);
		
		String text = Statistic.sso_id +"|"+ Statistic.product_id +"|"+ Statistic.category_id +"|"+ pagename +"|"+ 
				Statistic.referrer_url +"|"+ gettime() +"|"+ Statistic.content_id +"|"+ Statistic.source_of_content +"|"+ 
				Statistic.res +"|"+ Statistic.physical_id +"|"+ titlename +"|"+ Statistic.msisdn +"|"+ Statistic.os_version +"|"+
				Statistic.manufacturer +"|"+ Statistic.application_version +"|"+ Statistic.latitude +"|"+ Statistic.longitude
				+"\n";
		writeFile(text);
		
	}
	
	private static void writeFile(String text){
		
		try {
			//create new file
			if(file != null && !file.isFile())
				file.createNewFile();
		} catch (IOException e) {
			LOG.e(TAG, "Error : on writeFile : " + text, e);
		}
		
		
		try{
        	FileOutputStream fos = new FileOutputStream(file,true);
        	OutputStreamWriter writer = new OutputStreamWriter(fos);

        	writer.append(text);
        	Log.e("writer", text);
        	
        	writer.flush();
        	writer.close();
        	fos.flush();
        	fos.close();
        }catch(Exception e){
        	Log.e("Error", "file error");
        	e.printStackTrace();
        }
		
	}
	
	private static String gettime(){
		
		String time = UtilGenerator.getYear() + UtilGenerator.getMonth() + UtilGenerator.getDay() + UtilGenerator.gethour() + 
				UtilGenerator.getMin();
		return time;
		
	}

}
