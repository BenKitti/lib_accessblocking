package com.truelife.mobile.android.access_blocking.util;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;

import com.truelife.mobile.android.access_blocking.activity.DialogActivity;
import com.truelife.mobile.android.access_blocking.activity.DialogWebSheetActivity;
import com.truelife.mobile.android.util.file.SerializerObject;

public class AccessBlockingForService  {

	public Context context;
	
	public String state = "";
	
	int credit = 0;
	boolean check_credit = false;

	private String TAG = "AccessBlocking";

	
	public AccessBlockingForService(Context context){
		this.context = context;
	}
	
	
	public void checking(String appname,String version,String IMEI,String agent) {
		
		LOG.i(TAG , "Start AccessBlocking");
		
		String cn = TrueAppUtility.getSimOperator(context);
		
		if(String.valueOf(cn).equals("")||String.valueOf(cn).equalsIgnoreCase("null")){
			cn = "unknow";
		}
		
		if(TrueAppUtility.isNetworkAvailable(context)){
			LOG.i(TAG , "checkAccessBlocking");
			
			LOG.d(TAG , "appname : " + URLEncoder.encode(appname));
			LOG.d(TAG , "version : " + version);
			LOG.d(TAG , "NetworkCountry : " + URLEncoder.encode(TrueAppUtility.getNetworkCountryIso(context)));
			LOG.d(TAG , "SimOperator : " + URLEncoder.encode(cn));
			LOG.d(TAG , "agent : " + agent);
			LOG.d(TAG , "IMEI : " + IMEI);
			
			Blocking.checkAccessBlocking(threadHandler, URLEncoder.encode(appname), version , URLEncoder.encode(TrueAppUtility.getNetworkCountryIso(context)), URLEncoder.encode(cn) , agent, IMEI, "");
		}else{
			LOG.i(TAG , "Network not connected");
		}
	}
	
	private Handler threadHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case 200:{
					if(msg.obj!=null){
						LOG.e("data", String.valueOf(msg.obj));
						List<HashMap<String, Object>> list_data = (List<HashMap<String, Object>>) msg.obj;
						
						HashMap<String, Object> h = list_data.get(0);
						
						try {
							//credit = Integer.parseInt(String.valueOf(h.get("credit")));
						} catch (Exception e) {
							//credit = 0;
						}
						credit = 2;
						if(list_data.size()>1){
							HashMap<String, Object> control_data = list_data.get(1);
							if(String.valueOf(control_data.get("action")).equals("alert")){
								PopUpAlert(control_data);
							}else if(String.valueOf(control_data.get("action")).equals("websheet")){
								PopUpWebSheet(String.valueOf(control_data.get("url")).replace("url=", ""));
							}else if(String.valueOf(control_data.get("action")).equals("browser")){
								//PopUpWebSheet(String.valueOf(control_data.get("url")));
								Intent intent = new Intent(Intent.ACTION_VIEW);
								String urls = String.valueOf(control_data.get("url")).replace("url=", "");
								
								
								if( !urls.startsWith("market://" ) &&  !urls.startsWith("http")  ){
									urls = "http://"+urls;
								}
								
								intent.setData(Uri.parse(urls));
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		        				context.startActivity(intent);
		        				System.exit(0);
							}
						} 
					}else{
						msg.what = 400;
						threadHandler.sendMessage(msg);
					}
				}break;
				
			}
			super.handleMessage(msg);
		}
	};
	
	private void PopUpAlert(HashMap<String, Object> control_data){
//		dialog = new Dialog(context);
		
		Intent dialogIntent = new Intent(context, DialogActivity.class);
		
		dialogIntent.putExtra("control_data", SerializerObject.serializeObject((HashMap<String, Object>) control_data));
		dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(dialogIntent);
		
		
	}
	
	
	private void PopUpWebSheet(String url){
		Intent dialogIntent = new Intent(context, DialogWebSheetActivity.class);
		
		dialogIntent.putExtra("url", url);
		dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(dialogIntent);
		
		
	}
	
	
	
}
