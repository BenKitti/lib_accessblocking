package com.truelife.mobile.android.access_blocking.util;

import android.content.Context;
import android.util.DisplayMetrics;

public class ImageMetrics {
	static private ImageMetrics _instance;
	static public ImageMetrics getInstance(Context context) {
		if (_instance == null) {
			_instance = new ImageMetrics(context);
		}
		return _instance;
	}
	
	Context context;
	DisplayMetrics metrics;
	
	public ImageMetrics(Context context){
		this.context = context;
		this.metrics = this.context.getResources().getDisplayMetrics();
	}
	
	public int toDP(int dp){
		return (int) (metrics.density * dp + 0.5f);
	}
}
