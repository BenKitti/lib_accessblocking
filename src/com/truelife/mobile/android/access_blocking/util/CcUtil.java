/*
 * Copyright 2009 Codecarpet
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.truelife.mobile.android.access_blocking.util;

import java.io.InputStream;

import android.graphics.Picture;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class CcUtil {

    public static int rgbFloatToInt(float red, float green, float blue, float alpha) {
        int r = (int) (red * 255 + 0.5);
        int g = (int) (green * 255 + 0.5);
        int b = (int) (blue * 255 + 0.5);
        int a = (int) (alpha * 255 + 0.5);
        int value = ((a & 0xFF) << 24) | ((r & 0xFF) << 16) | ((g & 0xFF) << 8) | ((b & 0xFF) << 0);
        return value;
    }
    public static Drawable getDrawable(Class clazz, String path) {
        InputStream is = clazz.getClassLoader().getResourceAsStream(path);
        return Drawable.createFromStream(is, path);
    }
    
    public static BitmapDrawable getBitmapDrawable(Class clazz, String path) {
        InputStream is = clazz.getClassLoader().getResourceAsStream(path);
        return (BitmapDrawable) Drawable.createFromStream(is, path);
    }
    
    public static Picture getPicture(Class clazz, String path) {
        InputStream is = clazz.getClassLoader().getResourceAsStream(path);
        return Picture.createFromStream(is);
    }

}
