package com.truelife.mobile.android.access_blocking.util;

import android.util.Log;


public class LOG {

    public static final int DEPLOY = Log.ASSERT + 99;	//-- Deploy mode
    public static final int DEV = Log.VERBOSE ;			//-- DEV mode
    
    // Current log level
    public static int LOGLEVEL = DEPLOY;

    public static void setLogLevel(int logLevel) {
        LOGLEVEL = logLevel;
        LOG.i("TruelifeLog", "Changing log level to " + logLevel);
    }

    public static void setLogLevel(String logLevel) {
        if ("VERBOSE".equals(logLevel)) LOGLEVEL = Log.VERBOSE;
        else if ("DEBUG".equals(logLevel)) LOGLEVEL = Log.DEBUG;
        else if ("INFO".equals(logLevel)) LOGLEVEL = Log.INFO;
        else if ("WARN".equals(logLevel)) LOGLEVEL = Log.WARN;
        else if ("ERROR".equals(logLevel)) LOGLEVEL = Log.ERROR;
        LOG.i("TruelifeLog", "Changing log level to " + logLevel + "(" + LOGLEVEL + ")");
    }

    public static boolean isLoggable() {
        return (Log.ERROR >= LOGLEVEL);
    }
    
    public static boolean isLoggable(int logLevel) {
        return (logLevel >= LOGLEVEL);
    }

    public static void v(String tag, String s) {
        if (Log.VERBOSE >= LOGLEVEL) Log.v(tag, s);
    }

    public static void d(String tag, String s) {
        if (Log.DEBUG >= LOGLEVEL) Log.d(tag, s);
    }

    public static void i(String tag, String s) {
        if (Log.INFO >= LOGLEVEL) Log.i(tag, s);
    }

    public static void w(String tag, String s) {
        if (Log.WARN >= LOGLEVEL) Log.w(tag, s);
    }

    public static void e(String tag, String s) {
        if (Log.ERROR >= LOGLEVEL) Log.e(tag, s);
    }

    public static void v(String tag, String s, Throwable e) {
        if (Log.VERBOSE >= LOGLEVEL) Log.v(tag, s, e);
    }
    
    public static void d(String tag, String s, Throwable e) {
        if (Log.DEBUG >= LOGLEVEL) Log.d(tag, s, e);
    }

    public static void i(String tag, String s, Throwable e) {
        if (Log.INFO >= LOGLEVEL) Log.i(tag, s, e);
    }

    public static void w(String tag, String s, Throwable e) {
        if (Log.WARN >= LOGLEVEL) Log.w(tag, s, e);
    }

    public static void e(String tag, String s, Throwable e) {
        if (Log.ERROR >= LOGLEVEL) Log.e(tag, s, e);
    }

    public static void v(String tag, String s, Object... args) {
        if (Log.VERBOSE >= LOGLEVEL) Log.v(tag, String.format(s, args));
    }

    public static void d(String tag, String s, Object... args) {
        if (Log.DEBUG >= LOGLEVEL) Log.d(tag, String.format(s, args));
    }

    public static void i(String tag, String s, Object... args) {
        if (Log.INFO >= LOGLEVEL) Log.i(tag, String.format(s, args));
    }

    public static void w(String tag, String s, Object... args) {
        if (Log.WARN >= LOGLEVEL) Log.w(tag, String.format(s, args));
    }

    public static void e(String tag, String s, Object... args) {
        if (Log.ERROR >= LOGLEVEL) Log.e(tag, String.format(s, args));
    }

}
