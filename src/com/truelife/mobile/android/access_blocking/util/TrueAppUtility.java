package com.truelife.mobile.android.access_blocking.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.telephony.gsm.SmsManager;


public class TrueAppUtility {

	public static void printTelephonyInfo(Context ctx) {
		TelephonyManager TelephonyMgr = 
			(TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = TelephonyMgr.getDeviceId();
		String phoneNumber=TelephonyMgr.getLine1Number(); 
		String operator = TelephonyMgr.getNetworkOperator(); 
		String operatorName = TelephonyMgr.getNetworkOperatorName(); 
		
		LOG.v("IMEI", imei);
		LOG.v("PhoneNo", phoneNumber);
		LOG.v("Operator", operator);
		LOG.v("OperatorName", operatorName);
	}
	
	public static String getMSISDN(Context ctx){
		TelephonyManager TelephonyMgr = 
			(TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
		String MSISDN = "";
        try {
        	MSISDN = TelephonyMgr.getLine1Number().toString();
		} catch (Exception e) {
			MSISDN = "";
		}
        return MSISDN;
	}
	
	public static String getNetworkCountryIso(Context ctx){
		TelephonyManager TelephonyMgr = 
			(TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
		String NetworkCountryIso = "";
        try {
        	NetworkCountryIso = TelephonyMgr.getNetworkCountryIso();
		} catch (Exception e) {
			NetworkCountryIso = "";
		}
		if(String.valueOf(NetworkCountryIso).equals("")||String.valueOf(NetworkCountryIso).equalsIgnoreCase("null")){
			NetworkCountryIso = "unknow";
		}
		
        return NetworkCountryIso;
	}
	
	public static String getIMEI(Context ctx){
		TelephonyManager TelephonyMgr = 
			(TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = TelephonyMgr.getDeviceId();
        return imei;
	}
	
	public static String isTrueOperator(Context ctx) {
		TelephonyManager TelephonyMgr = 
			(TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);		
		return TelephonyMgr.getNetworkOperatorName(); 
	}
	
	public static boolean isTrueMoveOperator(Context context){
		
		try {
			String simOperator =   TrueAppUtility.getSimOperator(context).toLowerCase();
			return simOperator.contains("true") || simOperator.contains("52000");
		} catch (Exception e) {
			return false;
		} 
	}
	
	public static String getSimOperator(Context ctx) {
		TelephonyManager TelephonyMgr = 
			(TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);		
		
			String operator = TelephonyMgr.getSimOperatorName();
			
			if(String.valueOf(operator).equals("")||String.valueOf(operator).equalsIgnoreCase("null")){
                operator = isTrueOperator(ctx);
            }	
			
			if(String.valueOf(operator).equals("")||String.valueOf(operator).equalsIgnoreCase("null")){
				operator = "unknow";
			}
			
		return operator;
	}
	
	public static boolean exitIfOperatorNotTrue(Context ctx) {
		final Context context = ctx;
		
        if(!isTrueOperator(ctx).equalsIgnoreCase("true")&&!isTrueOperator(ctx).equalsIgnoreCase("orange")) {
        	AlertDialog.Builder builder = new AlertDialog.Builder(context);
        	builder.setMessage("ขออภัยค่ะ บริการนี้สำหรับลูกค้าทรูมูฟเท่านั้นค่ะ")
        	.setPositiveButton("OK", new DialogInterface.OnClickListener(){
        		public void onClick(DialogInterface dialog, int id) {
        			((Activity)context).finish();
        			System.exit(0);
        		}
        	}).setOnCancelListener(new OnCancelListener() {
				public void onCancel(DialogInterface arg0) {
					((Activity)context).finish();
        			System.exit(0);
				}
			})
        	.show();
        	
        	return false;
        } else return true;
	}
	
	public static boolean isNetworkAvailable(Context ctx) {
		final Context context = ctx;
		try {
			ConnectivityManager connectivity = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity == null) {
			} else {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null) {
					for (int i = 0; i < info.length; i++) {
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							//LOG.v("connect", info[i].toString());
							return true;
						}
					}
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	public static boolean exitIfNetworkNotAvailable(Context ctx) {
		final Context context = ctx;
		
        if(!isNetworkAvailable(ctx)) {
        	AlertDialog.Builder builder = new AlertDialog.Builder(context);
        	builder.setMessage("ไม่สามารถเชื่อมต่ออินเตอร์เน็ตได้ กรุณาตรวจสอบเน็ตเวิร์คของท่าน")
        	.setPositiveButton("OK", new DialogInterface.OnClickListener(){
        		public void onClick(DialogInterface dialog, int id) {
        			((Activity)context).finish();
        			System.exit(0);
        		}
        	})
        	.show();
        	
        	return false;
        } else return true;
	}
	public static void sendSMS(String phoneNumber, String message) {
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, null, null);
	}
	public static void callPhone(String phoneNumber, Context context) {
		Intent i = new Intent(Intent.ACTION_CALL);
		i.setData(Uri.parse("tel:" + phoneNumber));
		context.startActivity(i);
	}
	
	@TargetApi(3)
	public static String checkNeyworkType(Context ctx) {
		final Context context = ctx;
		try {
			ConnectivityManager connectivity = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity == null) {
			} else {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null) {
					for (int i = 0; i < info.length; i++) {
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							String type = "";
							int netType = info[i].getType();
							int netSubType = info[i].getSubtype();
							if(netType == ConnectivityManager.TYPE_WIFI){
								type = "wifi";
							}else if(netType == ConnectivityManager.TYPE_MOBILE && netSubType == TelephonyManager.NETWORK_TYPE_UMTS){
									type = "3g";
							}else if(netType == ConnectivityManager.TYPE_MOBILE && info[i].getSubtypeName().equals("HSPA")){
								type = "3g";
							}else if(netType == ConnectivityManager.TYPE_MOBILE && info[i].getSubtypeName().equals("HSDPA")){
								type = "3g";
							}else if(netType == ConnectivityManager.TYPE_MOBILE && info[i].getSubtypeName().equals("HSPA+ ")){
								type = "3g";
							}else if(netType == ConnectivityManager.TYPE_MOBILE && info[i].getSubtypeName().equals("HSUPA")){
								type = "3g";
							}else if(netType == ConnectivityManager.TYPE_MOBILE && info[i].getSubtypeName().equals("iDen")){
								type = "3g";
							}else if(netType == ConnectivityManager.TYPE_MOBILE && info[i].getSubtypeName().equals("LTE")){
								type = "3g";
							}else if(netType == ConnectivityManager.TYPE_MOBILE && info[i].getSubtypeName().equals("UMTS")){
								type = "3g";
							}else if(netType == ConnectivityManager.TYPE_MOBILE && netSubType == TelephonyManager.NETWORK_TYPE_EDGE){
								type = "edge";
							}else if(netType == ConnectivityManager.TYPE_MOBILE && netSubType == TelephonyManager.NETWORK_TYPE_GPRS){
								type = "gprs";
							}else{
								type = ""+netType;
							}
							//LOG.v("connect", info[i].toString());
							//LOG.v("type", type);
							return type;
						}
					}
				}
				return "not connect";
			}
		} catch (Exception e) {
			return "not connect";
		}
		return "not connect";
	}
}
