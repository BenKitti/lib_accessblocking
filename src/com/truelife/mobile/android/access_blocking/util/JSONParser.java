package com.truelife.mobile.android.access_blocking.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;



public class JSONParser {

	public static JSONObject fetch(String url) throws IOException {

		HttpClient httpclient = new DefaultHttpClient();
		
		//LOG.e("url", url);
		
		HttpPost httppost = new HttpPost(url);

		HttpResponse response;
		
		JSONObject dataArray = null;
		
		try {

			response = httpclient.execute(httppost);

			LOG.i("REST:Response Status line", response.getStatusLine().toString());

			HttpEntity entity = response.getEntity();

			if (entity != null) {

				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);

				// Parsing
				dataArray = new JSONObject(result);

				instream.close();
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return dataArray;
	}

		
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
}
