package com.truelife.mobile.android.access_blocking.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class BlockingPreferences {

	
	public static final int BLOCKING_CASE_GO_TO_APP = 0;
	public static final int BLOCKING_CASE_NEW_VERSION = 1;
	public static final int BLOCKING_CASE_FORCE_UPDATE = 2;
	
	private SharedPreferences appSharedPrefs;
    private Editor prefsEditor;

    private String LastUpdateStatus = "LastUpdateStatus";
    
    public BlockingPreferences(Context context)
    {
        this.appSharedPrefs = context.getSharedPreferences(context.getPackageName() + ".Blocking", 0);
        this.prefsEditor = appSharedPrefs.edit();
    }
    
    public boolean isShouldBlocking(int _updateStatus){
    	try {
    		
    		if(_updateStatus == BLOCKING_CASE_FORCE_UPDATE ){
    			updateBlockingStatus(_updateStatus);
    			return true;
    		}
    		
    		if(_updateStatus == appSharedPrefs.getInt(LastUpdateStatus, BLOCKING_CASE_GO_TO_APP)){
    			return false;
    		}else{
    			updateBlockingStatus(_updateStatus);
    			return true;
    		}
    		
		}catch (Exception e) {
			return true;
		}
    }
    
    public int getBlockingStatus(){
    	return appSharedPrefs.getInt(LastUpdateStatus, BLOCKING_CASE_GO_TO_APP);
    }
    
    public void updateBlockingStatus(int _updateStatus){
    	prefsEditor.putInt(LastUpdateStatus, _updateStatus );
        prefsEditor.commit();
    }
    
}
