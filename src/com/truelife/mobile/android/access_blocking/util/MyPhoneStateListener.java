package com.truelife.mobile.android.access_blocking.util;

import com.truelife.mobile.android.access_blocking.ChargingActivity;

import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;


public class MyPhoneStateListener extends PhoneStateListener {
	public void onCallStateChanged(int state,String incomingNumber){
		LOG.v("incomming state", ""+state);
		switch(state)
     	{
           case TelephonyManager.CALL_STATE_IDLE:
                LOG.d("DEBUG", "IDLE");
                if(ChargingActivity.ca!=null&&AccessBlocking.charging.state.equals("tel")){

                	ChargingActivity.ca.finish();
                	
                	ChargingActivity.ca.startActivity(AccessBlocking.charging.previousIntent);

                	AccessBlocking.charging.context.finish();
                	
                	AccessBlocking.charging.state="";
                }
                break;
           case TelephonyManager.CALL_STATE_OFFHOOK:
                LOG.d("DEBUG", "OFFHOOK");
                break;
           case TelephonyManager.CALL_STATE_RINGING:
        	   LOG.d("DEBUG", "RINGING");
        	   break;
     	}
	}	

}