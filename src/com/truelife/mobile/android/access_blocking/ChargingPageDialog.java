package com.truelife.mobile.android.access_blocking;

import java.net.MalformedURLException;

import android.app.Activity;


public class ChargingPageDialog extends ChargingDialog {

//    private static final String LOG = ChargingPageDialog.class.getSimpleName();
    
    private String kLoginURL = "";


    public ChargingPageDialog(Activity context,String url) {
        super(context);
        kLoginURL = url;
    }

    private void loadLoginPage() {
        try {
            loadURL(kLoginURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    //@Override
    protected void load() {
        loadLoginPage();
    }
}
